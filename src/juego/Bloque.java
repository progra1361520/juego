package juego;

import java.awt.Image;
import java.util.Random;
import entorno.Entorno;
import entorno.Herramientas;

public class Bloque {
	private double x; // Coordenada x del bloque
	private double y; // Coordenada y del bloque
	private double ancho; // Ancho del bloque
	private double alto; // Alto del bloque
	private Image imagen; // Imagen del bloque
	private boolean rompible; // Indica si el bloque es rompible

	// Constructor de la clase Bloque
	public Bloque(double x, double y, double ancho, double alto) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.rompible = new Random().nextBoolean(); // Asigna aleatoriamente si el bloque es rompible
		if (this.rompible == true) {
			this.imagen = Herramientas.cargarImagen("imagenes/Bloques/bloqueRompible.jpg");
		} else {
			this.imagen = Herramientas.cargarImagen("imagenes/Bloques/bloque.jpg");
		}
	}
	
	public Bloque(double x, double y, double ancho, double alto, boolean rompible) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.rompible = rompible;
		if (this.rompible == true) {
			this.imagen = Herramientas.cargarImagen("imagenes/Bloques/bloqueRompible.jpg");
		} else {
			this.imagen = Herramientas.cargarImagen("imagenes/Bloques/bloque.jpg");
		}
	}

	// Método para dibujar el bloque en la pantalla
	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(this.imagen, this.x, this.y, 0, 1); // Dibuja la imagen del bloque en la posición (x, y)
	}

	// Métodos getters para las coordenadas y dimensiones del bloque
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}

	// Método getter para verificar si el bloque es rompible
	public boolean getRompible() {
		return this.rompible;
	}



}

package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class ProyectilP {
	private int ancho; // Ancho del proyectil
	private int alto; // Alto del proyectil
	private int x; // Coordenada x del proyectil
	private int y; // Coordenada y del proyectil
	private double velocidad; // Velocidad del proyectil
	private Image proyectil; // Imagen del proyectil
	private boolean direccionDerecha; // Indica si el proyectil se mueve hacia la derecha

	// Constructor que recibe los parámetros necesarios para inicializar el
	// proyectil
	public ProyectilP(int ancho, int alto, int x, int y, double velocidad, boolean direccionDerecha) {
		this.ancho = ancho; // Inicializa el ancho del proyectil
		this.alto = alto; // Inicializa el alto del proyectil
		this.x = x; // Inicializa la coordenada x del proyectil
		this.y = y; // Inicializa la coordenada y del proyectil
		this.velocidad = velocidad; // Inicializa la velocidad del proyectil
		this.direccionDerecha = direccionDerecha; // Inicializa la dirección del proyectil
		actualizarImagen(); // Actualiza la imagen según la dirección del proyectil
	}

	// Método privado para actualizar la imagen del proyectil según su dirección
	private void actualizarImagen() {
		if (direccionDerecha) {
			// Carga la imagen del proyectil hacia la derecha
			this.proyectil = Herramientas.cargarImagen("imagenes/Princesa/ZHuhR.gif");
		} else {
			// Carga la imagen del proyectil hacia la izquierda
			this.proyectil = Herramientas.cargarImagen("imagenes/Princesa/ZHuhL.gif");
		}
	}

	// Método para mover el proyectil
	public void mover() {
		if (direccionDerecha) {
			// Mueve el proyectil hacia la derecha
			this.x += this.velocidad;
		} else {
			// Mueve el proyectil hacia la izquierda
			this.x -= this.velocidad;
		}
	}

	// Método para dibujar el proyectil en la pantalla
	public void dibujar(Entorno entorno) {
		// Dibuja la imagen del proyectil en la posición (x, y) con una escala de 0.5
		entorno.dibujarImagen(this.proyectil, this.x, this.y, 0, 0.5);
	}

	public boolean colision(ProyectilP proyectil) {
	    int margenColisionY = 10;

	    if (proyectil != null) {
	        boolean colisionaHorizontalmente = proyectil.getX() < this.x + this.ancho && proyectil.getX() + proyectil.getAncho() > this.x;
	        boolean colisionaVerticalmente = proyectil.getY() < this.y + this.alto + margenColisionY && proyectil.getY() + proyectil.getAlto() > this.y - margenColisionY;

	        if (colisionaHorizontalmente && colisionaVerticalmente) {
	            return true;
	        }
	    }
	    return false;
	}


	// Métodos getters para las coordenadas y dimensiones del proyectil

	// Obtiene la coordenada x del proyectil
	public int getX() {
		return this.x;
	}

	// Obtiene la coordenada y del proyectil
	public int getY() {
		return this.y;
	}

	// Obtiene el ancho del proyectil
	public int getAncho() {
		return this.ancho;
	}

	// Obtiene el alto del proyectil
	public int getAlto() {
		return this.alto;
	}
}

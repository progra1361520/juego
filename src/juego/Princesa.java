package juego;

import java.awt.Image;
import entorno.Herramientas;
import entorno.Entorno;

public class Princesa {
	private int x; // Coordenada x de la princesa
	private int y; // Coordenada y de la princesa
	private int ancho; // Ancho de la princesa
	private int alto; // Alto de la princesa
	private int velocidadY; // Velocidad vertical de la princesa (para el salto)
	private Image imagen; // Imagen de la princesa
	private boolean moviendoDerecha; // Indica si la princesa se está moviendo hacia la derecha
	private ProyectilP proyectil;
	private boolean flag_movimiento_proyectilp; // = false;

	private boolean saltando;
	private boolean subiendo;
	private boolean bajando;
	private int velocidadSalto;

	private int yInicial;

	private int alturaMaximaSalto;

	private int vidas;
	private int puntaje;
	private int enemigosEliminados;

	// Constructor de la clase Princesa
	public Princesa(int x, int y, int ancho, int alto, boolean enSalto, int velocidadY, int alturaSalto,
			boolean flag_movimiento_proyectilp, int alturaMaximaSalto) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.velocidadY = velocidadY;
		this.flag_movimiento_proyectilp = flag_movimiento_proyectilp;
		actualizarImagen();

		this.saltando = false;
		this.subiendo = false;
		this.bajando = false;
		this.velocidadSalto = 5;

		this.alturaMaximaSalto = alturaMaximaSalto;

		this.yInicial = this.y;

		this.vidas = 3;
		this.puntaje = 0;
		this.enemigosEliminados = 0;
	}

	// Método para actualizar la imagen de la princesa según su dirección
	private void actualizarImagen() {
		if (moviendoDerecha) {
			this.imagen = Herramientas.cargarImagen("imagenes/Princesa/Princess R.png");
		} else {
			this.imagen = Herramientas.cargarImagen("imagenes/Princesa/Princess L.png");
		}
	}

	// Método para dibujar la princesa en la pantalla
	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(this.imagen, this.x, this.y, 0, 0.1);
	}

	// Método para mover la princesa hacia la derecha
	public void moverDerecha() {
		this.x = this.x + 3;
		this.moviendoDerecha = true;
		actualizarImagen();
	}

	// Método para mover la princesa hacia la izquierda
	public void moverIzquierda() {
		this.x = this.x - 3;
		this.moviendoDerecha = false;
		actualizarImagen();
	}

	// metodos nuevos

	public void saltar() {
		this.saltando = true;
	}

	public void mover(Plataforma[] plataformas, int separacionEntrePlataformas) {
		if (this.saltando) {
			if (Math.abs(this.y - this.yInicial) < 1e-2 && !this.bajando) {
				this.subiendo = true;
				this.velocidadY = -this.velocidadSalto;
				this.bajando = false;
			}

			if (Math.abs(this.y - (this.yInicial - this.alturaMaximaSalto)) < 1e-2) {
				this.subiendo = false;
				this.bajando = true;
				this.velocidadY = this.velocidadSalto;
			}

			if (this.subiendo) {
				this.y += this.velocidadY;
			} else if (this.bajando) {
				this.y += this.velocidadY;
				if (colisionaAbajo(plataformas, 1)) {
					this.saltando = false;
					this.bajando = false;
					this.subiendo = false;
					this.yInicial = this.y;
				}
			}

			if (!this.bajando && chequearColisionEnSalto(plataformas, 0)) {
				this.subiendo = false;
				this.bajando = true;
				this.velocidadY = this.velocidadSalto;
			}

		}
		bajarDeNivel(plataformas, separacionEntrePlataformas);
	}

	private void bajarDeNivel(Plataforma[] plataformas, int separacionEntrePlataformas) {

		if (!this.saltando && !this.bajando) {

			if (!colisionaAbajo(plataformas, 5)) {
				this.subiendo = false;
				this.bajando = true;
				this.velocidadY = Math.abs(this.velocidadSalto);
			}
		}

		// Si el personaje está en el proceso de bajada
		if (this.bajando) {
			this.y += this.velocidadY;

			// Verifica si colisiona con una plataforma después de caer
			if (colisionaAbajo(plataformas, 5)) {
				this.bajando = false;
				this.yInicial = this.y;
			} else if (this.y >= this.yInicial + separacionEntrePlataformas) {

				this.y = this.yInicial + separacionEntrePlataformas;
				this.bajando = false;
				this.yInicial = this.y;
			}
		}
		if (this.yInicial == this.y) {
			this.saltando = false;
		}
	}

	public boolean colisionaAbajo(Plataforma[] plataformas, int margen) {
		for (int i = 0; i < plataformas.length; i++) {
			for (int j = 0; j < plataformas[i].getFila().length; j++) {
				if (plataformas[i].getBloqueActivo()[j]) {
					int plataformaY = (int) plataformas[i].getFila()[j].getY();
					int plataformaX = (int) plataformas[i].getFila()[j].getX();
					int plataformaAncho = (int) plataformas[i].getFila()[j].getAncho();

					if (Math.abs((this.y + this.alto) - plataformaY) <= margen
							&& this.x <= plataformaX + plataformaAncho / 2
							&& this.x >= plataformaX - plataformaAncho / 2) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public boolean chequearColision(Bloque bloque, int margen) {
		if (bloque.getRompible()) {

			return this.x < bloque.getX() + bloque.getAncho() && this.x + this.ancho > bloque.getX()
					&& this.y < bloque.getY() + bloque.getAlto() + margen && this.y + this.alto > bloque.getY();

		}
		return false;
	}

	// Metodo que chequee la colision de la princesa con el bloque, al chocar
	// cancela
	// el salto.
	public boolean chequearColisionEnSalto(Plataforma[] plataformas, int margen) {
		for (int i = 0; i < plataformas.length; i++) {
			for (int j = 0; j < plataformas[i].getFila().length; j++) {
				if (plataformas[i].getBloqueActivo()[j]) {

					int plataformaY = (int) plataformas[i].getFila()[j].getY();
					int plataformaX = (int) plataformas[i].getFila()[j].getX();
					int plataformaAncho = (int) plataformas[i].getFila()[j].getAncho();

					if (Math.abs(this.y - (plataformaY + plataformaAncho)) <= margen
							&& this.x >= plataformaX - plataformaAncho / 2
							&& this.x <= plataformaX + plataformaAncho / 2) {
						return true;
					}

				}
			}
		}
		return false;
	}

	public Dino colisionConDino(Dino dino) {

		if (dino != null) {
			boolean colisionaHorizontalmente = dino.getX() < this.x + this.ancho
					&& dino.getX() + dino.getAncho() > this.x;
			boolean colisionaVerticalmente = dino.getY() < this.y + this.alto && dino.getY() + dino.getAlto() > this.y;

			if (colisionaHorizontalmente && colisionaVerticalmente) {
				vidas = vidas - 1;
				dino = null;
			}
		}
		return dino;
	}

	public ProyectilP colisionConProyectil(ProyectilP proyectil) {

		if (proyectil != null) {
			boolean colisionaHorizontalmente = proyectil.getX() < this.x + this.ancho
					&& proyectil.getX() + proyectil.getAncho() > this.x;
			boolean colisionaVerticalmente = proyectil.getY() < this.y + this.alto
					&& proyectil.getY() + proyectil.getAlto() > this.y;

			if (colisionaHorizontalmente && colisionaVerticalmente) {
				vidas = vidas - 1;
				proyectil = null;
			}
		}
		return proyectil;
	}

	public void dispararProyectil() {
		boolean direccionDerecha = this.moviendoDerecha();
		this.proyectil = new ProyectilP(20, 20, (int) this.getX(), (int) this.getY(), 3, direccionDerecha);
		this.flag_movimiento_proyectilp = true;
	}

	public void moverProyectil(Entorno entorno) {
		if (this.proyectil != null) {
			this.getProyectil().mover();
			this.getProyectil().dibujar(entorno);
			if (this.getProyectil().getX() > entorno.ancho() || this.getProyectil().getX() < 0) {
				this.setFlag_movimiento_proyectilp(false);
			}
		}
	}

	public ProyectilP colisionEntreProyectiles(ProyectilP p) {

		if (this.proyectil != null) {
			if (this.proyectil.colision(p)) {
				this.proyectil = null;
				this.flag_movimiento_proyectilp=false;
				return null;
			}
		}
		return p;
	}

	public boolean ganar(int margen) {
		if (Math.abs(this.x - 15) <= margen && Math.abs(this.y - 65) <= margen)
			return true;
		return false;
	}

	public boolean perder() {
		if (this.vidas <= 0)
			return true;
		return false;
	}

	public boolean moviendoDerecha() {
		return this.moviendoDerecha;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getAlto() {
		return alto;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getAncho() {
		return ancho;
	}

	public boolean isFlag_movimiento_proyectilp() {
		return this.flag_movimiento_proyectilp;
	}

	public void setFlag_movimiento_proyectilp(boolean flag_movimiento_proyectilp) {
		this.flag_movimiento_proyectilp = flag_movimiento_proyectilp;
	}

	public ProyectilP getProyectil() {
		return this.proyectil;
	}

	public void setSaltando(boolean saltando) {
		this.saltando = saltando;
	}

	public void eliminarProyectil() {
		this.proyectil = null;
	}

	public int getPuntaje() {
		return puntaje;
	}

	public void setPuntaje(int puntaje) {
		this.puntaje = puntaje;
	}

	public int getEnemigosEliminados() {
		return enemigosEliminados;
	}

	public void setEnemigosEliminados(int enemigosEliminados) {
		this.enemigosEliminados = enemigosEliminados;
	}

	public int getVidas() {
		return vidas;
	}

}

package juego;

import entorno.Entorno;


public class Menu {
    private Entorno entorno;
    private boolean iniciarJuego;
    private Fondo fondo;
    
    public Menu(Entorno entorno, boolean iniciarJuego, Fondo fondo) {
        this.entorno = entorno;
        this.iniciarJuego = iniciarJuego;
        this.fondo = fondo;
    }

    public boolean iniciarJuego() {
        return iniciarJuego;
    }

    public void iniciarJuego(boolean iniciar) {
        this.iniciarJuego = iniciar;
    }

    public void mostrar() {
    	this.fondo.dibujarFondo(this.entorno);
    }

}


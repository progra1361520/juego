package juego;

import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Random;

import entorno.Entorno;
import entorno.Herramientas;

public class Dino {
	private int x; // Coordenada x del dinosaurio
	private int y; // Coordenada y del dinosaurio
	private int ancho; // Ancho del dinosaurio
	private int alto; // Alto del dinosaurio
	private int velocidad; // Velocidad del dinosaurio
	private Image imagen; // Imagen del dinosaurio
	private boolean mutado;
	private ProyectilP proyectil;
	private boolean flag_movimiento_proyectil;
	private boolean moviendoDerecha;
	private Random random;
	private static final int VELOCIDAD_CAIDA = 2; // Velocidad de caída del dinosaurio

	// Constructor de la clase Dino
	public Dino(int x, int y, int ancho, int alto, int velocidad) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.velocidad = velocidad;
		this.random = new Random();
		this.mutado = random.nextBoolean();
		this.flag_movimiento_proyectil = false;
		actualizarImagen(); // Actualiza la imagen del dinosaurio según la dirección del movimiento
	}

	// Método para actualizar la imagen del dinosaurio según la dirección de
	// movimiento
	private void actualizarImagen() {
		if (velocidad > 0) {
			this.imagen = Herramientas.cargarImagen("imagenes/Dino/The dino.png"); // Imagen cuando se mueve hacia la
																					// derecha
		} else {
			this.imagen = Herramientas.cargarImagen("imagenes/Dino/The dino left.png"); // Imagen cuando se mueve hacia
																						// la izquierda
		}
	}

	// Método para dibujar el dinosaurio en la pantalla
	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(this.imagen, this.x, this.y, 0, 0.25); // Dibuja la imagen del dinosaurio en la posición
																		// (x, y)
	}

	public void mover(Plataforma[] plataformas) {
		if (!estaSobreBloque(plataformas)) {
			this.y += VELOCIDAD_CAIDA;
		} else {
			this.x += this.velocidad;
		}
	}

	public boolean estaSobreBloque(Plataforma[] plataformas) {
		for (Plataforma plataforma : plataformas) {
			for (Bloque bloque : plataforma.getFila()) {
				if (bloque != null) {
					int dinoParteInferiorY = this.y + this.alto;
					int bloqueParteSuperiorY = (int) bloque.getY();
					int dinoCentroX = this.x + this.ancho / 2;
					int bloqueCentroX = (int) (bloque.getX() + bloque.getAncho() / 2);

					boolean estaSobreX = Math
							.abs(dinoCentroX - bloqueCentroX) <= (this.ancho / 2 + bloque.getAncho() / 2);
					boolean estaSobreY = Math.abs(dinoParteInferiorY - bloqueParteSuperiorY) <= VELOCIDAD_CAIDA;

					if (estaSobreX && estaSobreY) {
						return true; // El dinosaurio está sobre un bloque
					}
				}
			}
		}
		return false;
	}

	// Método para hacer rebotar al dinosaurio cuando alcanza los límites del
	// entorno
	public void rebotar(double anchoEntorno) {
		if ((this.x - this.ancho / 2 <= 0 || this.x + this.ancho / 2 >= anchoEntorno)) {
			this.velocidad = -this.velocidad; // Invierte la dirección de movimiento
			actualizarImagen(); // Actualiza la imagen cuando el dinosaurio rebota
		}
	}

	public static void eliminarDino(Dino dino) {
		dino = null;
	}

	public void dispararAleatorio() {

		double randomValue = random.nextDouble();
		double probabilidadDisparo = 0.05;

		if (randomValue <= probabilidadDisparo && mutado) {
			boolean direccionDerecha = this.moviendoDerecha;
			this.proyectil = new ProyectilP(20, 20, (int) this.getX(), (int) this.getY(), 3, direccionDerecha);
			this.flag_movimiento_proyectil = true;
		}

	}

	public void moverProyectil(Entorno entorno) {
		this.proyectil.mover();
		this.proyectil.dibujar(entorno);
		if (this.proyectil.getX() > entorno.ancho() || this.proyectil.getX() < 0) {
			this.flag_movimiento_proyectil = false;
		}
	}

	public boolean colisionDinoProyectil(ProyectilP proyectilp) {
		if (proyectilp != null) {
			if (Colision((int) this.getX(), (int) this.getY(), (int) this.getAncho(), (int) this.getAlto(),
					(int) proyectilp.getX(), (int) proyectilp.getY(), (int) proyectilp.getAncho(),
					(int) proyectilp.getAlto())) {
				return true;
			}
		}
		return false;
	}

	// Método auxiliar para verificar la colisión entre dos rectángulos
	public static boolean Colision(int x1, int y1, int b1, int h1, int x2, int y2, int b2, int h2) {
		Rectangle inter = new Rectangle();
		Point aux = new Point();

		// Coordenadas del rectángulo mayor
		inter.x = Math.min(x1 - b1 / 2, x2 - b2 / 2);
		inter.y = Math.min(y1 - h1 / 2, y2 - h2 / 2);

		// Coordenadas superiores del rectángulo mayor
		aux.x = Math.max(x1 + b1 / 2, x2 + b2 / 2);
		aux.y = Math.max(y1 + h1 / 2, y2 + h2 / 2);

		// Dimensiones del rectángulo mayor
		inter.width = aux.x - inter.x;
		inter.height = aux.y - inter.y;

		// Determinar si hay intersección
		if (inter.width < b1 + b2 && inter.height < h1 + h2) {
			return true;
		}
		return false;
	}

	// Métodos getters para las coordenadas y dimensiones del dinosaurio
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getAlto() {
		return alto;
	}

	public double getAncho() {
		return ancho;
	}

	public void setDireccion() {
		if (velocidad > 0) {
			this.moviendoDerecha = true;
		} else if (velocidad < 0) {
			this.moviendoDerecha = false;
		}
	}

	public boolean isFlag_movimiento_proyectil() {
		return flag_movimiento_proyectil;
	}

	public void setProyectil(ProyectilP proyectil) {
		this.proyectil = proyectil;
	}

	public ProyectilP getProyectil() {
		return this.proyectil;
	}

}

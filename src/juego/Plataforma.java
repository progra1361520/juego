package juego;

public class Plataforma {

	private int y;
	private Bloque[] fila;
	private int[] xBloques;
	private boolean[] bloqueRompible;
	private boolean[] bloqueActivo;
	private int altoBloques;
	private int anchoBloques;
	private int cantidadBloques;
	private int anchoPantalla;
	private boolean rompible;

	Plataforma(int y, int altoBloques, int cantidadBloques, int anchoPantalla) {

		this.y = y;
		this.cantidadBloques = cantidadBloques;
		this.anchoPantalla = anchoPantalla;
		this.fila = new Bloque[this.cantidadBloques];
		this.xBloques = new int[this.cantidadBloques];
		this.bloqueRompible = new boolean[this.cantidadBloques];
		this.bloqueActivo = new boolean[this.cantidadBloques];
		crearFila();
	}

	Plataforma(int y, int altoBloques, int cantidadBloques, int anchoPantalla, boolean rompible) {

		this.y = y;
		this.cantidadBloques = cantidadBloques;
		this.anchoPantalla = anchoPantalla;
		this.fila = new Bloque[this.cantidadBloques];
		this.xBloques = new int[this.cantidadBloques];
		this.bloqueRompible = new boolean[this.cantidadBloques];
		this.bloqueActivo = new boolean[this.cantidadBloques];
		this.rompible = rompible;
		crearFila(rompible);
	}

	private void crearFila() {

		this.anchoBloques = this.anchoPantalla / this.cantidadBloques;
		int xBloque = this.anchoBloques / 2;

		for (int i = 0; i < this.cantidadBloques; i++) {
			fila[i] = new Bloque(xBloque, this.y, this.anchoBloques, this.altoBloques);
			xBloques[i] = xBloque;
			bloqueRompible[i] = fila[i].getRompible();
			bloqueActivo[i] = true;
			xBloque = xBloque + this.anchoBloques;
		}
	}

	private void crearFila(boolean rompible) {

		this.anchoBloques = this.anchoPantalla / this.cantidadBloques;
		int xBloque = this.anchoBloques / 2;

		for (int i = 0; i < this.cantidadBloques; i++) {
			fila[i] = new Bloque(xBloque, this.y, this.anchoBloques, this.altoBloques, rompible);
			xBloques[i] = xBloque;
			bloqueRompible[i] = fila[i].getRompible();
			bloqueActivo[i] = true;
			xBloque = xBloque + this.anchoBloques;
		}
	}

	public void romperBloque(Princesa princesa) {
		for (int i = 0; i < fila.length; i++) {

			if (this.bloqueActivo[i] == true) {
				if (princesa.chequearColision(fila[i], 46)) {
					princesa.setSaltando(false);
					romperBloque(i);
				}
			}

		}
	}

	public int getY() {
		return y;
	}

	public Bloque[] getFila() {
		return fila;
	}

	public boolean[] getBloqueActivo() {
		return bloqueActivo;
	}

	public int getAltoBloques() {
		return altoBloques;
	}

	private void romperBloque(int n) {
		this.fila[n] = null;
		this.bloqueActivo[n] = false;
	}

	public boolean isRompible() {
		return rompible;
	}

}

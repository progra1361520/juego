package juego;

import java.awt.Color;
import java.util.Random;
import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;
import javax.sound.sampled.Clip;

public class Juego extends InterfaceJuego {
	// Instancias de objetos de la clase Entorno, Fondo, y Menu
	private Entorno entorno;
	private Fondo fondo;
	private Menu menu;

	private int cantidadPlataformas;
	private int separacionPlataformas;
	private int cantidadBloquesFila;
	private int altoBloques;
	private Plataforma[] plataformas;

	// Objetos Princesa y Dino
	private Princesa princesa;
	private Dino[] dinos;
	private Random random;

	// Sonidos del juego
	private Clip menu_music;
	private Clip game_music;
	private Clip disparo_sound;

	Juego() {
		// Inicializa el entorno del juego con el título, ancho y alto
		this.entorno = new Entorno(this, "Super Elizabeth Sis, Volcano Edition - Grupo ... - v1", 800, 600);
		this.menu = new Menu(entorno, false, new Fondo("../imagenes/Fondos/Clipchamp.gif", 400, 300, 0, 0.8));
		this.fondo = new Fondo("../imagenes/Fondos/cueva-oscura.jpg", 400, 300, 0, 1.3); // Inicializa el fondo

		//
		this.cantidadPlataformas = 5;
		this.cantidadBloquesFila = 20;
		this.altoBloques = 40;
		this.separacionPlataformas = 120;

		this.plataformas = new Plataforma[this.cantidadPlataformas];

		// creacion de plataformas
		int yPlataforma = this.entorno.alto() - this.altoBloques / 2;

		for (int i = 0; i < this.cantidadPlataformas; i++) {
			if (i == 0)
				plataformas[i] = new Plataforma(yPlataforma, this.altoBloques, this.cantidadBloquesFila,
						this.entorno.ancho(), false);
			else
				plataformas[i] = new Plataforma(yPlataforma, this.altoBloques, this.cantidadBloquesFila,
						this.entorno.ancho());
			yPlataforma = yPlataforma - this.separacionPlataformas;

		}

		// Inicializa la princesa en el entorno
		int altoPantalla = entorno.alto();
		int alturaPrincesa = 30;
		this.princesa = new Princesa(entorno.ancho() / 2, altoPantalla - this.altoBloques - alturaPrincesa / 2, 30, 30,
				false, 0, 100, false, 145);

		// crear niveles

		this.random = new Random();

		// Generar array de dinos
		this.dinos = generarDinos(8 + random.nextInt(1));

		// Iniciar el entorno
		this.entorno.iniciar();

	}

	// Genera un array de dinos con posiciones y velocidades aleatorias
	private Dino[] generarDinos(int cantidad) {
		Dino[] dinos = new Dino[cantidad];
		for (int i = 0; i < cantidad; i++) {
			int x = random.nextInt(780) + 10;
			int y = 155 + (i / 2) * 120; // Distribuir en diferentes niveles
			int ancho = 20;
			int alto = 50;
			int velocidad = (i % 2 == 0) ? -2 : 2;
			dinos[i] = new Dino(x, y, ancho, alto, velocidad);
		}
		return dinos;
	}

	private void romperBloque() {
		for (int i = 0; i < this.plataformas.length; i++) {
			this.plataformas[i].romperBloque(this.princesa);
		}
	}

	// Método que se ejecuta en cada tick del juego
	public void tick() {
		// Si el juego no ha empezado, mostrar el menú
		if (!menu.iniciarJuego()) {
			menu.mostrar();
			if (menu_music == null) {
				menu_music = Herramientas.cargarSonido("sonidos/MenuS.wav");
				menu_music.start();
			}
//
//			// Empezar el juego al presionar ENTER
			if (entorno.sePresiono(entorno.TECLA_ENTER)) {
				menu.iniciarJuego(true);
				menu_music.stop();
				game_music = Herramientas.cargarSonido("sonidos/InGame.wav");
				game_music.start();
			}

			// Salir del juego al presionar CTRL
			if (entorno.sePresiono(entorno.TECLA_CTRL)) {
				System.exit(0);
			}
			return;
		}

		// Dibujar el fondo y los bloques
		this.fondo.dibujarFondo(this.entorno);

		for (int i = 0; i < plataformas.length; i++) {
			Bloque[] fila = plataformas[i].getFila();
			for (int j = 0; j < fila.length; j++) {
				if (plataformas[i].getBloqueActivo()[j] == true) {
					fila[j].dibujar(this.entorno);
				}

			}
		}
		
		//Dibujar texto en la pantalla
		this.entorno.cambiarFont("Arial", 20, Color.WHITE);
		this.entorno.escribirTexto("Puntaje: "+this.princesa.getPuntaje(), 550, 20);
		this.entorno.escribirTexto("Enemigos eliminados: "+this.princesa.getEnemigosEliminados(), 550, 40);
		this.entorno.escribirTexto("Vidas: "+this.princesa.getVidas(), 550, 60);
		this.entorno.escribirTexto("Salida", 5, 20);

		// Dibujar la princesa y moverla
		this.princesa.dibujar(this.entorno);
		if (this.entorno.estaPresionada(this.entorno.TECLA_DERECHA)
				&& this.princesa.getX() + this.princesa.getAncho() / 2 < this.entorno.ancho()) {
			this.princesa.moverDerecha();
		}
		if (this.entorno.estaPresionada(this.entorno.TECLA_IZQUIERDA)
				&& this.princesa.getX() - this.princesa.getAncho() / 2 > 0) {
			this.princesa.moverIzquierda();
		}

		if (this.entorno.sePresiono('x')) {
			this.princesa.saltar();
		}

		// Disparar proyectil al presionar ESPACIO
		if (entorno.sePresiono('c') && !this.princesa.isFlag_movimiento_proyectilp()) {
			this.princesa.dispararProyectil();
			disparo_sound = Herramientas.cargarSonido("sonidos/disparo sound.wav");
			disparo_sound.start();
		}

		// Mover el proyectil y dibujarlo
		if (this.princesa.isFlag_movimiento_proyectilp()) {
			this.princesa.moverProyectil(this.entorno);

		}

		// Dibujar y mover dinos
		for (Dino dino : this.dinos) {
			if (dino != null) { // Verificar si el dino no está muerto antes de dibujar y mover
				dino.dibujar(this.entorno);
				dino.mover(this.plataformas);
				dino.rebotar(this.entorno.ancho());
				dino.setDireccion();
				if (!dino.isFlag_movimiento_proyectil()) {
					dino.dispararAleatorio();
				}

			}
		}

		for (Dino dino : this.dinos) {
			if (dino != null) {
				if (dino.getProyectil() != null) {
					dino.moverProyectil(this.entorno);
				}
			}
		}

		// Verificar colisión entre proyectil y dinos
		colisionDinoProyectil();

		this.princesa.mover(this.plataformas, this.separacionPlataformas);

		// Romper bloques si hay colisión con la princesa
		romperBloque();
		
		//Colision entre la princesa y los dinosaurios
		colisionPrincesaDino();
		
		//Colision entre la princesa y los proyectiles de los dinos
		colisionPrincesaProyectil();
		
		//Colision entre proyectiles
		colisionEntreProyectiles();

		// gana cuando llega al extremo izquierdo de la fila superior
		if (this.princesa.ganar(5)) {
			System.out.println("Ganaste!");
			System.exit(0);
		}
		
		//perder
		if(this.princesa.perder()) {
			System.out.println("Perdiste!");
			System.exit(0);
		}

	}

	// Verifica colisiones entre el proyectil y los dinos
	public void colisionDinoProyectil() {
		if (this.princesa.getProyectil() == null) {
			return; // No hay proyectil disponible para verificar la colisión
		}
		for (int i = 0; i < dinos.length; i++) {
			Dino dino = dinos[i];
			if (dino != null && dino.colisionDinoProyectil(this.princesa.getProyectil())) {
				dinos[i] = null;
				this.princesa.eliminarProyectil();
				this.princesa.setFlag_movimiento_proyectilp(false);
				this.princesa.setEnemigosEliminados(this.princesa.getEnemigosEliminados() + 1);
				this.princesa.setPuntaje(this.princesa.getPuntaje() + 2);
			}
		}
	}
	
	private void colisionPrincesaDino() {
		for (int i = 0; i < dinos.length; i++) {
			dinos[i] = this.princesa.colisionConDino(dinos[i]);
		}
	}
	
	private void colisionPrincesaProyectil() {
		for (int i = 0; i < dinos.length; i++) {
			if(dinos[i] != null) {
				dinos[i].setProyectil(this.princesa.colisionConProyectil(dinos[i].getProyectil()));
			}
		}
	}
	
	private void colisionEntreProyectiles() {
		for (int i = 0; i < dinos.length; i++) {
			if(dinos[i] != null) {
				dinos[i].setProyectil(this.princesa.colisionEntreProyectiles(dinos[i].getProyectil()));
			}
		}
	}

	// Método main para iniciar el juego
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}
}
